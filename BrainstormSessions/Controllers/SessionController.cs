﻿using System.Reflection;
using System.Threading.Tasks;
using BrainstormSessions.Core.Interfaces;
using BrainstormSessions.ViewModels;
using log4net;
using Microsoft.AspNetCore.Mvc;
using Serilog;

namespace BrainstormSessions.Controllers
{
    public class SessionController : Controller
    {
        private readonly IBrainstormSessionRepository _sessionRepository;
        private readonly ILog _logger;
        public SessionController(IBrainstormSessionRepository sessionRepository)
        {
            _sessionRepository = sessionRepository;
            _logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        }

        public async Task<IActionResult> Index(int? id)
        {
            _logger.Info("Index execution start (Log4net)");
            Log.Logger.Information("Index execution start (Serilog)");

            if (!id.HasValue)
            {
                _logger.Error("Id hasn't value (Log4net)");
                Log.Logger.Error("Id hasn't value (Serilog)", id);
                return RedirectToAction(actionName: nameof(Index),
                    controllerName: "Home");
            }

            _logger.Debug($"Session search start with id {id} (Log4net)");
            Log.Logger.Debug($"Session search start with id {id} (Serilog)");

            var session = await _sessionRepository.GetByIdAsync(id.Value);
            if (session == null)
            {
                _logger.Warn("Session not found (Log4net)");
                Log.Logger.Warning("Session not found (Serilog)");
                return Content("Session not found.");
            }

            var viewModel = new StormSessionViewModel()
            {
                DateCreated = session.DateCreated,
                Name = session.Name,
                Id = session.Id
            };

            _logger.Debug($"Returning session view model {viewModel.Name} (Log4net)");
            Log.Logger.Debug($"Returning session view model {viewModel.Name} (Serilog)", viewModel);

            return View(viewModel);
        }
    }
}
